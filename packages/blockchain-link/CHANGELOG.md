# 1.0.0-rc3
#### changes
- Added possibility to export workers as a main thread module (using webpack build)
- ./src/workers/common.ts changed to class for multiple instance usage

# 1.0.0-rc1
- First release