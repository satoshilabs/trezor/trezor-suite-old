import styled from 'styled-components';

const StepFooterWrapper = styled.div`
    display: flex;
    margin-top: auto;
    margin-bottom: auto;
    text-align: center;
    flex: 1;
`;

export default StepFooterWrapper;
