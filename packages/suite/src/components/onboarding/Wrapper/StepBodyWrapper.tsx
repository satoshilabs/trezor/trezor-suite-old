import styled from 'styled-components';

const StepBodyWrapper = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    text-align: center;
    width: 100%;
    /* justify-content: space-around; */
`;

export default StepBodyWrapper;
