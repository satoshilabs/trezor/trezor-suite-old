export const SET_ADDITIONAL_FORM_VISIBILITY = '@wallet_send_form/set_additional_form_visibility';

export const HANDLE_ADDRESS_CHANGE = '@wallet_send_form/handle_address_change';
export const HANDLE_AMOUNT_CHANGE = '@wallet_send_form/handle_amount_change';
export const SET_MAX = '@wallet_send_form/set_max';

export const HANDLE_SELECT_CURRENCY_CHANGE = '@wallet_send_form/handle_local_currency_change';
export const HANDLE_FIAT_VALUE_CHANGE = '@wallet_send_form/handle_fiat_value_change';
export const HANDLE_FEE_VALUE_CHANGE = '@wallet_send_form/handle_fee_value_change';
export const HANDLE_CUSTOM_FEE_VALUE_CHANGE = '@wallet_send_form/handle_custom_fee_value_change';

export const CLEAR = '@wallet_send_form/clear';

// btc specific
export const BTC_ADD_RECIPIENT = '@wallet_send_form/btc-add-recipient';
export const BTC_REMOVE_RECIPIENT = '@wallet_send_form/btc-remove-recipient';

// ethereum specific

// xrp specific
export const XRP_HANDLE_DESTINATION_TAG_CHANGE =
    '@wallet_send_form/xrp_handle-destination-tag-change';
