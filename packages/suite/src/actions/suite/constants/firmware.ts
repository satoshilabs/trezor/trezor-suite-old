export const SET_UPDATE_STATUS = '@suite/set-update-status';
export const ENABLE_REDUCER = '@suite/enable-firmware-reducer';
export const RESET_REDUCER = '@suite/reset-firmware-reducer';
