import React from 'react';
import styled from 'styled-components';
import { Props } from './Container';
import CustomFee from '../CustomFee';

const Wrapper = styled.div`
    display: flex;
    flex: 1;
    flex-direction: column;
`;

const Row = styled.div`
    padding: 0 0 30px 0;
    display: flex;

    &:last-child {
        padding: 0;
    }
`;

const NetworkTypeBitcoin = (props: Props) => (
    <Wrapper>
        <Row>
            <CustomFee
                errors={props.send.customFee.error}
                customFee={props.send.customFee.value}
                sendFormActions={props.sendFormActions}
            />
        </Row>
    </Wrapper>
);

export default NetworkTypeBitcoin;
