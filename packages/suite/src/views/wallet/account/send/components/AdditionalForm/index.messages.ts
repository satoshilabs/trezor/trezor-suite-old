import { defineMessages } from 'react-intl';

const definedMessages = defineMessages({
    TR_CUSTOM_FEE_IS_NOT_SET: {
        id: 'TR_CUSTOM_FEE_IS_NOT_SET',
        defaultMessage: 'Fee is not set',
    },
    TR_CUSTOM_FEE_IS_NOT_VALID: {
        id: 'TR_CUSTOM_FEE_IS_NOT_NUMBER',
        defaultMessage: 'Fee is not a number',
    },
});

export default definedMessages;
